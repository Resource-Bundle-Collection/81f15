# 彻底关闭Win10自动更新并随时恢复的解决方案

本文提供了一个详细的解决方案，帮助用户彻底关闭Windows 10的自动更新功能，并且可以在需要时随时恢复该功能。该解决方案适用于希望控制系统更新的用户，尤其是在更新频繁导致系统不稳定或需要保持特定系统版本的情况下。

## 解决方案概述

该解决方案分为两个主要部分：

1. **手动备份、删除及恢复usosvc服务**：
   - 备份注册表：usosvc服务的大部分信息存储在注册表中，备份这些信息以便在需要时恢复。
   - 删除usosvc服务：通过禁用和删除usosvc服务来阻止Windows 10的自动更新。
   - 恢复usosvc服务：在需要恢复自动更新时，通过导入备份的注册表文件和重新创建usosvc服务来恢复功能。

2. **使用脚本自动备份、删除及恢复usosvc服务**：
   - 提供了一个VBS脚本，自动完成备份、删除和恢复usosvc服务的操作，简化用户操作步骤。

## 使用步骤

### 手动操作

1. **备份注册表**：
   - 打开注册表编辑器（按Win+R，输入`regedit`并回车）。
   - 找到路径`HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\usosvc`，右键点击`usosvc`并选择“导出”。
   - 保存备份文件，记住文件路径以便后续恢复使用。

2. **删除usosvc服务**：
   - 以管理员身份打开Windows PowerShell。
   - 输入以下命令禁用和删除usosvc服务：
     ```
     sc.exe stop usosvc
     sc.exe delete usosvc
     ```

3. **恢复usosvc服务**：
   - 以管理员身份打开Windows PowerShell。
   - 输入以下命令重新创建usosvc服务：
     ```
     sc.exe create usosvc binpath="c:\windows\system32\svchost.exe -k netsvcs -p -s UsoSvc" type=share start=auto error=normal tag=no depend=rpcss displayname="更新 Orchestrator 服务"
     ```
   - 导入之前备份的注册表文件。
   - 输入以下命令启动usosvc服务：
     ```
     sc.exe start usosvc
     sc.exe query usosvc
     ```

### 使用脚本

1. **下载脚本文件**：
   - 下载提供的VBS脚本文件（文件名为`管理Win10自动更新v1.1.vbs`）。

2. **运行脚本**：
   - 双击运行脚本，按照提示操作即可。

## 注意事项

- 在执行删除usosvc服务之前，务必进行注册表备份，以防操作失误导致系统问题。
- 恢复usosvc服务时，确保导入正确的注册表备份文件。
- 使用脚本操作时，请确保脚本文件来源可靠，避免潜在的安全风险。

通过以上步骤，用户可以有效地控制Windows 10的自动更新功能，确保系统稳定性和满足特定需求。